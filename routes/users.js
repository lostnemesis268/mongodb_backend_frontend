const express = require('express')
const router = express.Router()
// const usersController = require('../controller/UsersController')
const User = require('../models/User')
/* GET users listing. */
router.get('/', (req, res, next) => {
  // res.json(usersController.getUsers())
  User.find({}).then(function (users) {
    res.json(users)
  }).catch(function (err) {
    res.status(500).send(err)
  })
})
router.get('/:id', async (req, res, next) => {
  const { id } = req.params
  // res.json(usersController.getUser(id))
  // User.findById(id).then(function (user) {
  //   res.json(user)
  // }).catch(function (err) {
  //   res.status(500).send(err)
  // })
  try {
    const user = await User.findById(id)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})
router.post('/', (req, res, next) => {
  const payload = req.body
  // res.json(usersController.addUser(payload))
  const user = new User(payload)
  try {
    user.save()
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})
router.put('/', (req, res, next) => {
  const payload = req.body
  // res.json(usersController.updateUser(payload))
  try {
    const user = User.updateOne({ _id: payload._id }, payload)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})
router.delete('/:id', (req, res, next) => {
  const { id } = req.params
  // res.json(usersController.deleteUser(id))
  try {
    const user = User.deleteOne({ _id: id })
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

module.exports = router
